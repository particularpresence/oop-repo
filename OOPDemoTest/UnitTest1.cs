using System;
using Xunit;
using Moq;
using Solid;
using System.Reflection;

namespace OOPDemoTest
{
    public class UnitTest1
    {
        [Fact]
        public void BA_GetCalculations_VerifyCalledOne()
        {

            Mock<Program.ICalculation> calculatorMock = new Mock<Program.ICalculation>();
            Program.BusinessAnalysis sut = new Program.BusinessAnalysis(calculatorMock.Object);

            sut.GetCalculation(1, 2);

            calculatorMock.Verify(calc => calc.RunCalculation(1, 2), Times.Once);

            Assert.Equal(1, 1);
        }

        [Fact]
        public void RandysSupersizedFunction()
        {

            Assembly.Load("OOP Demo");
           // Mock<Program.ICalculation> calculatorMock = new Mock<Program.ICalculation>();
            Program.CalculatorFactory sut = new Program.CalculatorFactory();

            Program.ICalculation expectedResult = new Program.Division();

            Program.ICalculation actualResult = sut.GetCalculator(new Program.MyCalculationRequirement()
            {
                MySpecialCase = 0,
                TypeOfCalculation = "divide"
            });

            Assert.IsType<Program.Division> (actualResult); 

        }
    }
}

/*public class BusinessAnalysis
{
    //Dependency Inversion
    private readonly ICalculation _myCalculation;
    public BusinessAnalysis(ICalculation myCalculation)
    {
        _myCalculation = myCalculation;
    }

    public double GetCalculation(double num1, double num2)
    {
        double result = _myCalculation.RunCalculation(num1, num2);
        return result;
  }
}*/