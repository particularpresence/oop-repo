﻿using System;
using System.Linq;
using System.Reflection;

namespace Solid
{
    public class Program
    {
        public interface ICalculation
        {
            bool CanCalculate(MyCalculationRequirement typeOfCaulculation);
            double RunCalculation(double num1, double num2);
        }

        public abstract class BaseCalculation : ICalculation
        {
            public abstract bool CanCalculate(MyCalculationRequirement typeOfCaulculation);

            public abstract double RunCalculation(double num1, double num2);
        }

        public class MyCalculationRequirement
        {
            public string TypeOfCalculation { get; set; }
            public int MySpecialCase { get; set; }
        }

        //Liskov Substitution & Open-Close Principle
        public class Addition : BaseCalculation
        {
            public override bool CanCalculate(MyCalculationRequirement typeOfCaulculation)
            {
                return string.Equals(typeOfCaulculation.TypeOfCalculation, "add", StringComparison.OrdinalIgnoreCase);
            }

            public override double RunCalculation(double num1, double num2)
            {
                return Add(num1, num2);
            }

            private double Add(double num1, double num2)
            {
                return num1 + num2;
            }
        }

        //Liskov Substitution & Open-Close Principle
        public class Subtraction : BaseCalculation
        {
            public override bool CanCalculate(MyCalculationRequirement typeOfCaulculation)
            {
                return string.Equals(typeOfCaulculation.TypeOfCalculation, "subtract", StringComparison.OrdinalIgnoreCase);
            }

            public override double RunCalculation(double num1, double num2)
            {
                return Subtract(num1, num2);
            }
            private double Subtract(double num1, double num2)
            {
                return num1 - num2;
            }
        }

        //Liskov Substitution & Open-Close Principle
        public class Multiplication : BaseCalculation
        {
            public override bool CanCalculate(MyCalculationRequirement typeOfCaulculation)
            {
                return string.Equals(typeOfCaulculation.TypeOfCalculation, "multiply", StringComparison.OrdinalIgnoreCase);
            }

            public override double RunCalculation(double num1, double num2)
            {
                return Multiply(num1, num2);
            }
            private double Multiply(double num1, double num2)
            {
                return num1 * num2;
            }
        }

        //Liskov Substitution & Open-Close Principle
        public class Division : BaseCalculation
        {
            public override bool CanCalculate(MyCalculationRequirement typeOfCaulculation)
            {
                return string.Equals(typeOfCaulculation.TypeOfCalculation, "divide", StringComparison.OrdinalIgnoreCase);
            }

            public override double RunCalculation(double num1, double num2)
            {
                return Divide(num1, num2);
            }
            private double Divide(double num1, double num2)
            {
                return num1 / num2;
            }
        }

        //Liskov Substitution & Open-Close Principle
        public class MySpecialCalculation : BaseCalculation
        {
            public override bool CanCalculate(MyCalculationRequirement typeOfCaulculation)
            {
                //eg
                return int.Equals(typeOfCaulculation.MySpecialCase, 3);
            }

            public override double RunCalculation(double num1, double num2)
            {
                return SomethingSpecial(num1, num2);
            }
            private double SomethingSpecial(double num1, double num2)
            {
                return num1 % num2;
            }
        }

        public class BusinessAnalysis
        {
            //Dependency Inversion
            private readonly ICalculation _myCalculation;
            public BusinessAnalysis(ICalculation myCalculation)
            {
                _myCalculation = myCalculation;
            }

            public double GetCalculation(double num1, double num2)
            {
                double result = _myCalculation.RunCalculation(num1, num2);  

                return result;
            }
        }

        //Factory pattern using reflection
        public class CalculatorFactory
        {
            public ICalculation GetCalculator(MyCalculationRequirement calculationRequirement)
            {
                //REFLECTION

                //Gets all informtion about the currect application
                var currentAssembly = Assembly.GetAssembly(typeof(Program));

                //Gets all classes in the application
                var allClasses = currentAssembly.GetTypes();

                //Finds all classes we are concered about --in our case, children of ICalculation
                var matchingClasses = allClasses.Where(t => t.IsSubclassOf(typeof(BaseCalculation)));

                //Finds the the of the corect child, by calling 'CanCalculate' on each and passing in the requirement parameter until on returns true.
                var theCorrectCalculator = matchingClasses.FirstOrDefault(x => ((BaseCalculation)Activator.CreateInstance(x)).CanCalculate(calculationRequirement));

                //Initialises the correct calculator
                var instanceOFTheCorrectCalculator = (ICalculation)Activator.CreateInstance(theCorrectCalculator);

                return instanceOFTheCorrectCalculator;

                //OR
                //return (BaseCalculation)Activator.CreateInstance(Assembly.GetEntryAssembly().GetTypes().Where(t => t.IsSubclassOf(typeof(ICalculation))).First(x => ((ICalculation)Activator.CreateInstance(x)).CanCalculate(typeOfCalculator)));
            }
        }

        static void Main(string[] args)
        {
            while (true)
            {
                Console.WriteLine("Enter your desired calculation");
                var myCalculationReq = new MyCalculationRequirement();

                myCalculationReq.TypeOfCalculation = Console.ReadLine();

                //Dependency inversion
                //CalculatorFactory decides which calculation to implement
                ICalculation myCalculation = new CalculatorFactory().GetCalculator(myCalculationReq);
                double result1 = new BusinessAnalysis(myCalculation).GetCalculation(3, 5);
                Console.WriteLine(
                    $"The type of {nameof(myCalculation)} is: {myCalculation.GetType()}\nThe result produced is: {result1}\n\n");

            }
        }
    }
}
